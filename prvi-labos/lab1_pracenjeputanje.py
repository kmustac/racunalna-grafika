import time
import numpy as np
import pyglet
from pyglet.window import mouse, key
from pyglet.gl import *


config = pyglet.gl.Config(double_buffer=True)
window = pyglet.window.Window(config=config, width=1600, height=900)


def enter_values(x):
    file = open(x, 'r')
    line = file.readline()
    r = []
    while line:
        if line.startswith('r'):
            ro = line[2:-1]
            ro = ro.split(' ')
            r.append((float(ro[0]), float(ro[1]), float(ro[2])))
        line = file.readline()
    file.close()
    return r


def loadView(x):
    file = open(x, 'r')
    line = file.readline()
    while line:
        if line.startswith("lookAt"):
            lo = line.strip().split(' ')
            camx, camy, camz, lookx, looky, lookz, viewx, viewy, viewz = float(lo[1]), float(lo[2]), float(lo[3]), \
                                                                         float(lo[4]), float(lo[5]), float(lo[6]), \
                                                                         float(lo[7]), float(lo[8]), float(lo[9])
        line = file.readline()
    file.close()
    return camx, camy, camz, lookx, looky, lookz, viewx, viewy, viewz


def loadBody(x):
    file = open(x, 'r')
    line = file.readline()
    kutevi = []
    poligoni = []
    while line:
        if line.startswith('v'):
            thing = line[2:-1]
            thing = thing.split(' ')
            kutevi.append((float(thing[0]), float(thing[1]), float(thing[2])))
        elif line.startswith('f'):
            thing = line[2:-1].strip()
            thing = thing.split(' ')
            poligoni.append((int(thing[0])-1, int(thing[1])-1, int(thing[2])-1))
        line = file.readline()
    file.close()
    return kutevi, poligoni


def clear_screen():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    #glMatrixMode(GL_PROJECTION)
    #glLoadIdentity()
    #glColor3f(0.0, 0.0, 0.0)


def nacrtaj_poligon():
    global r
    glColor3f(1.0, 0.0, 0.0)
    glScalef(20, 20, 20)
    glBegin(GL_LINES)
    for ind in range(0, len(r)):
        if ind + 1 < len(r):
            glVertex3f(r[ind][0], r[ind][1], r[ind][2])
            glVertex3f(r[ind+1][0], r[ind+1][1], r[ind+1][2])
    glEnd()


def izracunaj_koord_bspline():
    global r
    t = 0
    segment_size = 4
    num_segment = len(r) - 3
    B = np.array([
        [-1, 3, -3, 1],
        [3, -6, 3, 0],
        [-3, 0, 3, 0],
        [1, 4, 1, 0]
    ])

    # first segment test
    t_coords = []
    for segment in range(num_segment):
        t = 0
        while t <= 1:
            t_array = np.array([t**3, t**2, t, 1])
            r_array = np.array(r[segment:segment_size+segment])
            # print(r_array)
            p = t_array * (1/6)
            p = np.matmul(p, B)
            p = np.matmul(p, r_array)
            t_coords.append((p, segment))
            t += DRAW_STEP
    return t_coords


def izracunaj_vect_tangente():
    global r
    t = 0
    segment_size = 4
    num_segment = len(r) - 3
    B = np.array([
        [-1, 3, -3, 1],
        [2, -4, 2, 0],
        [-1, 0, 1, 0]
    ])
    vect = []
    for segment in range(num_segment):
        t = 0
        while t <= 1:
            t_array = np.array([t**2, t, 1])
            r_array = np.array(r[segment:segment_size+segment])
            p = t_array * (1/2)
            p = np.matmul(p, B)
            p = np.matmul(p, r_array)
            vect.append(p)
            t += DRAW_STEP
    return vect


def izracunaj_vect_normale():
    global r
    t = 0
    segment_size = 4
    num_segment = len(r) - 3
    B = np.array([
        [-1, 3, -3, 1],
        [3, -6, 3, 0],
        [-3, 0, 3, 0],
        [1, 4, 1, 0]
    ])
    vect = []
    for segment in range(num_segment):
        t = 0
        while t <= 1:
            t_array = np.array([6*t, 2, 0, 0])
            r_array = np.array(r[segment:segment_size + segment])
            p = np.matmul(t_array, (1/6)*B)
            p = np.matmul(p, r_array)
            vect.append(p)
            t += DRAW_STEP
    return vect


def nacrtaj_tangente(t_coords):
    vect = izracunaj_vect_tangente()
    glColor3f(1.0, 0.0, 0.0)
    glBegin(GL_LINES)
    for i in range(len(t_coords)):
        coord, segment = t_coords[i]
        color = colors[segment % len(colors)]
        glColor3f(color[0], color[1], color[2])
        v = vect[i]
        glVertex3f(coord[0], coord[1], coord[2])
        glVertex3f(coord[0]+v[0]*ALPHA, coord[1] + v[1]*ALPHA, coord[2]+v[2]*ALPHA)
    glEnd()


def nacrtaj_bspline():
    t_coords = izracunaj_koord_bspline()
    glColor3f(1.0, 0.0, 0.0)
    glBegin(GL_LINE_STRIP)
    for coord, segment in t_coords:
        color = colors[segment % len(colors)]
        glColor3f(color[0], color[1], color[2])
        glVertex3f(coord[0], coord[1], coord[2])
    glEnd()
    nacrtaj_tangente(t_coords)


def nacrtaj_krivulju_poligon(x, y):
    global r
    glBegin(GL_LINE_STRIP)
    for ind in range(0, len(r)):
        glVertex3f(r[ind][0], r[ind][1], r[ind][2])
    glEnd()
    nacrtaj_bspline()


def nacrtaj_tijelo(scaling_matrix:None):
    global kutevi, poligoni
    for poly in poligoni:
        glBegin(GL_POLYGON)
        for ind in poly:
            kut = kutevi[ind]
            if scaling_matrix is not None:
                kut = np.array(kut)
                kut = np.matmul(kut, scaling_matrix)
            glVertex3f(kut[0], kut[1], kut[2])
        glEnd()


def animiraj_tijelo():
    t_coords = izracunaj_koord_bspline()
    s = np.array([0, 1, 0])
    tang_vect = izracunaj_vect_tangente()

    for i in range(len(t_coords)):
        coord, segment = t_coords[i]
        e = tang_vect[i]

        os = np.cross(s, e)
        fi = np.degrees(np.arccos((np.dot(s, e)/(np.linalg.norm(s) * np.linalg.norm(e)))))

        glTranslatef(coord[0], coord[1], coord[2])
        glRotatef(fi, os[0], os[1], os[2])
        nacrtaj_tijelo(None)
        glRotatef(-fi, os[0], os[1], os[2])
        glTranslatef(-coord[0], -coord[1], -coord[2])

        window.flip()
        time.sleep(0.01)
        clear_screen()


def animiraj_tijelo_DCM():
    t_coords = izracunaj_koord_bspline()
    w_array = izracunaj_vect_tangente()
    u_array = izracunaj_vect_normale()

    for i in range(len(t_coords)):
        coord, segment = t_coords[i]
        w = w_array[i]
        u = u_array[i]

        v = np.cross(w, u)

        R = np.vstack((w, u, v)).T
        R = np.linalg.inv(R)
        glTranslatef(coord[0], coord[1], coord[2])
        nacrtaj_tijelo(R)
        glTranslatef(-coord[0], -coord[1], -coord[2])

        window.flip()
        time.sleep(0.01)
        clear_screen()


@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, width / float(height), .1, 1000)
    glMatrixMode(GL_MODELVIEW)
    return pyglet.event.EVENT_HANDLED


@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glClearColor(0, 0, 0, 1)
    glLoadIdentity()
    gluLookAt(camx, camy, camz, lookx, looky, lookz, viewx, viewy, viewz)

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
    glEnable(GL_CULL_FACE)
    glCullFace(GL_BACK)

    glColor3f(1.0, 1.0, 1.0)
    glTranslatef(-1.5, -0.5, 0)
    # nacrtaj_tijelo()
    #animiraj_tijelo()
    animiraj_tijelo_DCM()
    glTranslatef(1.5, 0.5, 0)
    glDisable(GL_CULL_FACE)

    glColor3f(1.0, 1.0, 1.0)
    glTranslatef(-1, -0.5, 0)
    nacrtaj_krivulju_poligon(0, 0)
    # nacrtaj_bspline()
    glTranslatef(1, 0.5, 0)


@window.event
def on_key_press(symbol, modiifiers):
    global camx, camy, camz
    if symbol == key.UP:
        camy += 1
    elif symbol == key.DOWN:
        camy -= 1
    elif symbol == key.LEFT:
        camx -= 1
    elif symbol == key.RIGHT:
        camx += 1


colors = [(1.0, 0.0, 0.0), (0.0, 1.0, 0.0), (0.0, 0.0, 1.0)]
ALPHA = 0.3
DRAW_STEP = 0.025   # smooth kocka
#DRAW_STEP = 0.1  # ostali objekti
filename_curve = "tocke_krivulje.txt"
r = enter_values(filename_curve)
kutevi, poligoni = loadBody("kocka.obj")
izracunaj_koord_bspline()
camx, camy, camz, lookx, looky, lookz, viewx, viewy, viewz = loadView(filename_curve)
pyglet.app.run()
