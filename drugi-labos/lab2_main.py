from camera import Camera
from particle_engine import ParticleEngine
import time
import numpy as np
import pyglet
import _ctypes as ct
from pyglet.window import mouse, key
from pyglet.gl import *
from pyglet import image
from math import cos, sin
from math import *

config = pyglet.gl.Config(double_buffer=True)
window = pyglet.window.Window(config=config, width=1600, height=1200)


def room():
    picture = image.load("christmas_room.jpg")
    texture = picture.get_texture()
    glBindTexture(texture.target, texture.id)
    glEnable(GL_TEXTURE_2D)

    glTranslatef(-15, -8, -20)
    sf = 2
    glScalef(1/sf, 1/sf, 1/sf)

    glBegin(GL_QUADS)
    glTexCoord2f(0, 0)
    glVertex3f(0.0, 0.0, -20.0)
    glTexCoord2f(0, 1)
    glVertex3f(0.0, 50.0, -20.0)
    glTexCoord2f(1, 1)
    glVertex3f(100.0, 50.0, -20.0)
    glTexCoord2f(1, 0)
    glVertex3f(100.0, 0.0, -20.0)
    glEnd()

    glScalef(sf, sf, sf)
    glTranslatef(15, 8, 20)

    glDisable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, 0)


def coords():
    glColor3f(1.0, 0.0, 0.0)
    glBegin(GL_LINES)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(3.0, 0.0, 0.0)
    glEnd()
    glColor3f(0.0, 1.0, 0.0)
    glBegin(GL_LINES)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 3.0, 0.0)
    glEnd()
    glColor3f(0.0, 0.0, 1.0)
    glBegin(GL_LINES)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 0.0, 3.0)
    glEnd()
    glColor3f(1.0, 1.0, 1.0)


def cube():
    glBegin(GL_LINE_LOOP)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 0.0, 1.0)
    glVertex3f(0.0, 1.0, 0.0)
    glVertex3f(0.0, 1.0, 1.0)
    glVertex3f(1.0, 0.0, 0.0)
    glVertex3f(1.0, 0.0, 1.0)
    glVertex3f(1.0, 1.0, 0.0)
    glVertex3f(1.0, 1.0, 1.0)
    glEnd()


def set_sparkler_engine(x, y, z):
    pe = ParticleEngine(x, y, z, num_particles=50)
    pe.load_particle_texture("spark.png")

    return pe


def set_snow_engine(x, y, z):
    pe = ParticleEngine(x, y, z, num_particles=100,
                        v_min=0.1, v_max=2,
                        time_s=5, time_e=15,
                        downward=True)
    pe.load_particle_texture("showflake.png")
    return pe

@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, width / float(height), .1, 1000)
    glMatrixMode(GL_MODELVIEW)
    return pyglet.event.EVENT_HANDLED


@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glClearColor(0, 0, 0, 1)
    glLoadIdentity()

    camx, camy, camz = camera.ociste[0], camera.ociste[1], camera.ociste[2]
    lookx, looky, lookz = camera.glediste[0], camera.glediste[1], camera.glediste[2]
    viewx, viewy, viewz = camera.view[0], camera.view[1], camera.view[2]
    gluLookAt(camx, camy, camz, lookx, looky, lookz, viewx, viewy, viewz)

    scaling_factor = 1
    glScalef(scaling_factor, scaling_factor, scaling_factor)

    #room()
    coords()
    #cube()
    for engine in engines:
        engine.draw_particles(camera)

    glScalef(1/scaling_factor, 1/scaling_factor, 1/scaling_factor)

@window.event
def on_mouse_drag(x, y, dx, dy, button, modifiers):
    global camera
    if button == mouse.LEFT:
        camera.translate_glediste(dx/30, dy/30)
    elif button == mouse.RIGHT:
        if modifiers & key.MOD_SHIFT:
            sparkler2.initial_pos = np.array([dx/30, dy/30, 0])
        else:
            sparkler1.initial_pos = np.array([dx/30, dy/30, 0])
            #sparkler1.initial_pos = np.array([x / window.width/2 - 1, y / window.height/2 - 1, 0])


if __name__ == "__main__":
    angle_rot_lr, angle_rot_ud, rotation_axis = 0, 0, "y"
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    engines, update_engines = [], []

    keys = key.KeyStateHandler()
    window.push_handlers(keys)

    camera = Camera(keys)
    camera.set_ociste(3.0, 3.0, 15.0)
    camera.set_glediste(0.0, 0.0, 0.0)
    camera.set_viewup(0, 1, 0)

    sparkler = set_sparkler_engine(0, 0, 0)

    sparkler1 = set_sparkler_engine(-5.0, 0.0, 0.0)
    sparkler2 = set_sparkler_engine(5.0, 0.0, 0.0)

    snowEngine = set_snow_engine(0.0, 10.0, 0.0)

    engines.append(sparkler1)
    engines.append(sparkler2)

    engines.append(snowEngine)
    update_engines.append(sparkler1)
    update_engines.append(sparkler2)

    update_engines.append(snowEngine)


    pyglet.clock.schedule_interval(snowEngine.update_position, 1/144)

    for engine in update_engines:
        pyglet.clock.schedule_interval(engine.update, 1/144)

    pyglet.clock.schedule_interval(camera.update_camera, 1/30)
    pyglet.app.run()
