import numpy as np
from math import sin, cos
from pyglet.window import key

class Camera:
    def __init__(self, keystate=None):
        self.cam = np.array([0.0, 0.0, 0.0])
        self.ociste = np.array([0.0, 0.0, 0.0])
        self.glediste = np.array([0.0, 0.0, 0.0])
        self.view = np.array([0, 0, 0])

        self.angle_rot_lr = 0
        self.angle_rot_ud = 0
        self.angle_step = 0.05
        self.translation_amount = 0.25

        self.keystate = keystate

    def set_ociste(self, x, y, z):
        self.cam = np.array([x, y, z])
        self.ociste = np.array([x, y, z])

    def set_glediste(self, x, y, z):
        self.glediste = np.array([x, y, z])

    def set_viewup(self, x, y, z):
        self.view = np.array([x, y, z])

    def rotate_camera(self, angle, rotation_axis):
        if rotation_axis == "x":
            R = np.array([
                [1, 0, 0],
                [0, cos(angle), sin(angle)],
                [0, -1 * sin(angle), cos(angle)]
            ])
        elif rotation_axis == "y":
            R = np.array([
                [cos(angle), 0, -1 * sin(angle)],
                [0, 1, 0],
                [sin(angle), 0, cos(angle)]
            ])
        new_ociste = np.dot(self.cam, R)
        self.ociste = new_ociste.copy()

    def translate_camera(self, amount, translation_axis):
        if translation_axis == "x":
            self.ociste[0] += amount
        elif translation_axis == "y":
            self.ociste[1] += amount
        elif translation_axis == "z":
            self.ociste[2] += amount
        #print(self.ociste)

    def translate_glediste(self, amountx, amounty):
        self.glediste[0] += amountx
        self.glediste[1] += amounty

    def translate_towards(self, amount, vector):
        self.ociste += amount*vector

    def update_camera(self, dt):
        vector = self.ociste - self.glediste
        vector = vector / np.linalg.norm(vector)
        if self.keystate[key.W]:
            self.translate_towards(-1*self.translation_amount, vector)
            #self.translate_camera(-1*self.translation_amount, "z")
        elif self.keystate[key.S]:
            self.translate_towards(self.translation_amount, vector)
            #self.translate_camera(self.translation_amount, "z")
        elif self.keystate[key.A]:
            self.translate_camera(-1*self.translation_amount, "x")
        elif self.keystate[key.D]:
            self.translate_camera(self.translation_amount, "x")
        elif self.keystate[key.Q]:
            self.translate_camera(self.translation_amount, "y")
        elif self.keystate[key.E]:
            self.translate_camera(-1*self.translation_amount, "y")
        elif self.keystate[key.RIGHT]:
            self.angle_rot_lr += self.angle_step
            self.rotate_camera(self.angle_rot_lr, "y")
        elif self.keystate[key.LEFT]:
            self.angle_rot_lr -= self.angle_step
            self.rotate_camera(self.angle_rot_lr, "y")
        elif self.keystate[key.UP]:
            self.angle_rot_ud -= self.angle_step
            self.rotate_camera(self.angle_rot_ud, "x")
        elif self.keystate[key.DOWN]:
            self.angle_rot_ud += self.angle_step
            self.rotate_camera(self.angle_rot_ud, "x")

