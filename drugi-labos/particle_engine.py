import numpy as np
import pyglet
import _ctypes as ct
from pyglet.window import mouse, key
from pyglet.gl import *
from pyglet import image
from math import cos, sin
import random
from camera import Camera


class ParticleEngine:
    def __init__(self, inital_x, initial_y, initial_z, v_min=7, v_max=10, time_s=0.05, time_e=0.3, num_particles=10,
                 downward=False):
        self.texture = None
        self.time_s = time_s
        self.time_e = time_e
        self.v_min = v_min
        self.v_max = v_max
        self.downward = downward
        self.initial_pos = np.array([inital_x, initial_y, initial_z])
        self.num_particles = num_particles
        self.particle_init_pos = [
            self.initial_pos.copy() for i in range(num_particles)
        ]
        self.particle_trans_factors = [
            np.array([0.0, 0.0, 0.0]) for i in range(num_particles)
        ]
        self.particle_vectors = [
            np.random.uniform(-1, 1, size=3) for i in range(num_particles)
        ]
        self.particle_death_times = [
            random.uniform(time_s, time_e) for i in range(num_particles)
        ]
        self.particle_live_times = [
            0 for i in range(num_particles)
        ]
        self.velocity = [
            np.random.uniform(v_min, v_max, size=3) for i in range(num_particles)
        ]
        self.normalize_particle_vectors()
        if downward:
            self.set_downward_movement()

        self.time_passed = 0

    def load_particle_texture(self, texture_fn):
        picture = image.load(texture_fn)
        self.texture = picture.get_texture()

    def draw_particle(self, translation_factor, initpos, camera):
        mov = initpos + translation_factor

        # Definiranje podloge za teksturu
        quad_verteces = np.array([
            [0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0],
            [1.0, 1.0, 0.0],
            [1.0, 0.0, 0.0]
        ])

        # Izracun rotacije podloge prema ocistu

        # Normala podloge
        quad_normal = np.cross((quad_verteces[1] - quad_verteces[0]), (quad_verteces[3] - quad_verteces[0]))
        quad_normal = quad_normal / np.linalg.norm(quad_normal)

        # Vektor od podloge prema ocistu
        vektor = quad_verteces.sum()/4 - camera.ociste
        #vektor = vektor / np.linalg.norm(vektor)

        # Os i kut rotacije
        os = np.cross(quad_normal, vektor)
        os = os / np.linalg.norm(os)
        fi = np.degrees(np.arccos(np.dot(quad_normal, vektor) / (np.linalg.norm(vektor) * np.linalg.norm(quad_normal))))

        glBindTexture(self.texture.target, self.texture.id)
        glEnable(GL_TEXTURE_2D)
        glEnable(GL_BLEND)
        glDepthMask(True)

        glTranslatef(mov[0], mov[1], mov[2])
        glRotatef(fi, os[0], os[1], os[2])
        glBegin(GL_QUADS)
        glTexCoord2f(0, 0)
        glVertex3f(quad_verteces[0][0], quad_verteces[0][1], quad_verteces[0][2])
        glTexCoord2f(0, 1)
        glVertex3f(quad_verteces[1][0], quad_verteces[1][1], quad_verteces[1][2])
        glTexCoord2f(1, 1)
        glVertex3f(quad_verteces[2][0], quad_verteces[2][1], quad_verteces[2][2])
        glTexCoord2f(1, 0)
        glVertex3f(quad_verteces[3][0], quad_verteces[3][1], quad_verteces[3][2])
        glEnd()

        glRotatef(-fi, os[0], os[1], os[2])
        glTranslatef(-mov[0], -mov[1], -mov[2])

        glDisable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, 0)
        glDisable(GL_BLEND)
        glDepthMask(False)

    def draw_particles(self, camera):
        for i in range(len(self.particle_trans_factors)):
            self.draw_particle(self.particle_trans_factors[i], self.particle_init_pos[i], camera)

    def normalize_particle_vectors(self):
        for i in range(len(self.particle_vectors)):
            self.particle_vectors[i] = self.particle_vectors[i]/np.linalg.norm(self.particle_vectors[i])

    def update(self, dt):
        self.time_passed += dt
        self.update_particle_times(dt)
        indexes_to_remove = []

        for i in range(len(self.particle_death_times)):
            if self.particle_live_times[i] > self.particle_death_times[i]:
                indexes_to_remove.append(i)

        if len(indexes_to_remove) > 0:
            indexes_to_remove.sort(reverse=True)
            self.remove_particles(indexes_to_remove)
            self.add_new_particles(len(indexes_to_remove))

        for i in range(len(self.particle_trans_factors)):
            ds = self.velocity[i] * self.particle_vectors[i] * dt
            self.particle_trans_factors[i] += ds
            # self.particle_trans_factors[i] -= 9.81*dt**2  #TODO: neki oblik gravitacije

        #if self.time_passed > 5:
        #    print("Unscheduled")
        #    pyglet.clock.unschedule(self.update)

    def update_position(self, dt):
        frequency = 10
        s = sin(2*3.14*frequency*dt)
        ds = 2*3.14*frequency*cos(2*3.14*frequency*dt)
        self.initial_pos[0] += 0.2
        if self.initial_pos[0] > 5:
            self.initial_pos[0] = -5

    def set_downward_movement(self):
        self.particle_vectors = [
            np.array([0, -1, 0]) for i in range(self.num_particles)
        ]
        self.normalize_particle_vectors()

    def remove_particles(self, indexes):
        #print("Deleting these:")
        #print(indexes)
        #print("From these")
        #print("Len_pt: " + str(len(self.particle_live_times)))
        for index in indexes:
            #print(index)
            del self.particle_live_times[index]
            del self.particle_death_times[index]
            #print("Len_pt_after:" + str(len(self.particle_times)))
            del self.particle_vectors[index]
            del self.particle_trans_factors[index]
            del self.velocity[index]

            del self.particle_init_pos[index]

    def add_new_particles(self, num_to_add):
        for i in range(num_to_add):
            self.particle_live_times.append(0)
            self.particle_death_times.append(random.uniform(self.time_s, self.time_e))
            self.particle_vectors.append(np.random.uniform(-1, 1, size=3))
            self.particle_trans_factors.append(np.array([0.0, 0.0, 0.0]))
            self.velocity.append(np.random.uniform(self.v_min, self.v_max, size=3))
            self.particle_init_pos.append(self.initial_pos.copy())
        if self.downward:
            self.set_downward_movement()
        else:
            self.normalize_particle_vectors()

    def update_particle_times(self, value):
        self.particle_live_times = [x + value for x in self.particle_live_times]
