import numpy as np
import random
import math
from pyglet.gl import *


class Chromosome:
    """
    Class that models a single chromosome used in the genetic algorithm.
    """
    def __init__(self, lb: float, ub: float, size: int = 2):
        """
        Class constructor. Given the lower and upper chromosome bounds as well as the size of the chromosome,
        it generates a field of random numbers in the given bounds. It also loads and stores the cube .obj file that
        visually represents the chromosome.
        :param lb: Lower bound
        :param ub: Upper bound
        :param size: Number of fields in the chromosome
        """
        self.lower_bound = lb
        self.upper_bound = ub
        self.size = size

        self.kutevi, self.poligoni = self.loadBody("kocka.obj")

        self.values = []
        for i in range(self.size):
            self.values.append(self.lower_bound + random.random()*(self.upper_bound - self.lower_bound))

        self.color_orange = np.array([1, 165/255, 0])
        self.color_blue = np.array([65/255, 105/255, 1])

    # Mutations
    def mutate_additive(self, chromosome, mutation_prob: float):
        """
        Additive mutation. A random number is added to a random field in the chromosome.
        :param chromosome: Chromosome to mutate
        :param mutation_prob: Mutation probability
        :return: Mutated chromosome
        """
        new_values = []
        for x in chromosome.values:
            if random.random() <= mutation_prob:
                p = x + self.lower_bound + random.random()*(self.upper_bound - self.lower_bound)

                if p > self.upper_bound:
                    p = self.upper_bound
                elif p < self.lower_bound:
                    p = self.lower_bound
            else:
                p = x
            new_values.append(p)
        new_chromosome = Chromosome(self.lower_bound, self.upper_bound, self.size)
        new_chromosome.values = new_values.copy()
        return new_chromosome

    def mutate_random(self, chromosome, mutation_prob: float):
        """
        Mutation that replaces a random field with a random number within bounds.
        :param chromosome: Chromosome to mutate
        :param mutation_prob: Mutation probability
        :return: Mutated chromosome
        """
        new_values = []
        for x in chromosome.values:
            if random.random() <= mutation_prob:
                p = self.lower_bound + random.random()*(self.upper_bound - self.lower_bound)
            else:
                p = x
            new_values.append(p)
        new_chromosome = Chromosome(self.lower_bound, self.upper_bound, self.size)
        new_chromosome.values = new_values.copy()
        return new_chromosome

    # Crossovers
    def cross_uniform(self, parent1, parent2):
        """
        Uniform crossover.
        :param parent1: First parent
        :param parent2: Second parent
        :return: Child chromosome
        """
        new_values = []
        for i in range(0, parent1.size):
            if random.random() < 0.5:
                new_values.append(parent1.values[i])
            else:
                new_values.append(parent2.values[i])
        new_chromosome = Chromosome(self.lower_bound, self.upper_bound, self.size)
        new_chromosome.values = new_values.copy()
        return new_chromosome

    def cross_arithmetic(self, parent1, parent2):
        """
        Simple arithmetic recombination.
        :param parent1: First parent
        :param parent2: Second parent
        :return: Child chromosome
        """
        new_chromosome = Chromosome(self.lower_bound, self.upper_bound, self.size)
        new_values = []
        for i in range(parent1.size):
            new_values.append((parent1.values[i] + parent2.values[i])*0.5)
        new_chromosome.values = new_values
        return new_chromosome

    def draw(self, function):
        """
        Draws the selected chromosome.
        :param function: Function to determine the y offset
        :return: None
        """
        y_offset = function(self.values)
        sf_k = 1/10
        # offset interpolation
        goodness = abs(y_offset - function.min)
        goodness = 1 / goodness
        color = goodness*self.color_blue + (1 - goodness)*self.color_orange
        glColor3f(color[0], color[1], color[2])

        glScalef(sf_k, sf_k, sf_k)
        self.draw_cube(self.values[0], y_offset, self.values[1])
        glScalef(1 / sf_k, 1 / sf_k, 1 / sf_k)

    def draw_color(self, function, color):
        y_offset = function(self.values)
        sf_k = 1 / 10
        glColor3f(color[0], color[1], color[2])
        glScalef(sf_k, sf_k, sf_k)
        self.draw_cube(self.values[0], y_offset, self.values[1])
        glScalef(1 / sf_k, 1 / sf_k, 1 / sf_k)

    def draw_cube(self, x: float, y: float, z: float):
        """
        Draws a single cube at given coordinates. The given coordinates are the center of the cube.
        :param x: X coordinate
        :param y: Y coordinate
        :param z: Z coordinate
        :return: None
        """
        glTranslatef(x - 0.5, y - 0.5, z - 0.5)
        for poly in self.poligoni:
            glBegin(GL_POLYGON)
            for ind in poly:
                kut = self.kutevi[ind]
                glVertex3f(kut[0], kut[1], kut[2])
            glEnd()
        glTranslatef(-x + 0.5, -y + 0.5, -z + 0.5)

    def loadBody(self, x: str):
        """
        Loads the body (in this case the cube) into the program from a .obj file
        :param x: .obj filename
        :return: Angles and polygons
        """
        file = open(x, 'r')
        line = file.readline()
        kutevi = []
        poligoni = []
        while line:
            if line.startswith('v'):
                thing = line[2:-1]
                thing = thing.split(' ')
                kutevi.append((float(thing[0]), float(thing[1]), float(thing[2])))
            elif line.startswith('f'):
                thing = line[2:-1].strip()
                thing = thing.split(' ')
                poligoni.append((int(thing[0]) - 1, int(thing[1]) - 1, int(thing[2]) - 1))
            line = file.readline()
        file.close()
        return kutevi, poligoni

    def __str__(self):
        ispis = "["
        for x in self.values:
            ispis += str(x) + ", "
        ispis = ispis[:-2]
        ispis += "]"
        return ispis

    def __eq__(self, other):
        return self.values == other.values
