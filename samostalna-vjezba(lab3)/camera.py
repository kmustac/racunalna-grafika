import numpy as np
from math import sin, cos
from pyglet.window import key


class Camera:
    """
    Class for manipulating the camera object in the scene.
    """
    def __init__(self, keystate: dict = None):
        """
        Camera constructor. Initial parameters are all set to 0 or arrays of zeros.
        :param keystate: Keystate tracker
        """
        self.cam = np.array([0.0, 0.0, 0.0])
        self.ociste = np.array([0.0, 0.0, 0.0])
        self.glediste = np.array([0.0, 0.0, 0.0])
        self.view = np.array([0, 0, 0])

        self.angle_rot_lr = 0
        self.angle_rot_ud = 0
        self.angle_step = 0.05
        self.translation_amount = 0.25

        self.keystate = keystate

    def set_ociste(self, x: float, y: float, z: float):
        """
        Sets the camera position at given coordinates.
        :param x: X coordinate
        :param y: Y coordinate
        :param z: Z coordinate
        :return: None
        """
        self.cam = np.array([x, y, z])
        self.ociste = np.array([x, y, z])

    def set_glediste(self,x: float, y: float, z: float):
        """
        Sets the camera looking position at given coordinates.
        :param x: X coordinate
        :param y: Y coordinate
        :param z: Z coordinate
        :return: None
        """
        self.glediste = np.array([x, y, z])

    def set_viewup(self, x: float, y: float, z: float):
        """
        Sets the viewup vector.
        :param x: X axis
        :param y: Y axis
        :param z: Z axis
        :return: None
        """
        self.view = np.array([x, y, z])

    def rotate_camera(self, angle: float, rotation_axis: str):
        """
        Rotates the camera around the given rotation axis by a given angle.
        :param angle: Angle by which to rotate the camera
        :param rotation_axis: Around which axis to rotate the camera
        :return: None
        """
        if rotation_axis == "x":
            R = np.array([
                [1, 0, 0],
                [0, cos(angle), sin(angle)],
                [0, -1 * sin(angle), cos(angle)]
            ])
        elif rotation_axis == "y":
            R = np.array([
                [cos(angle), 0, -1 * sin(angle)],
                [0, 1, 0],
                [sin(angle), 0, cos(angle)]
            ])
        new_ociste = np.dot(self.cam, R)
        self.ociste = new_ociste.copy()

    def translate_camera(self, amount: float, translation_axis: str):
        """
        Translates the camera by the given amount along the given axis.
        :param amount: Amount by which to translate the camera
        :param translation_axis: X, Y, or Z axis.
        :return: None
        """
        if translation_axis == "x":
            self.ociste[0] += amount
        elif translation_axis == "y":
            self.ociste[1] += amount
        elif translation_axis == "z":
            self.ociste[2] += amount
        #print(self.ociste)

    def translate_glediste(self, amountx: float, amounty: float):
        """
        Translates the camera looking point by given x and y amounts in the z plane. Used for mouse movement.
        :param amountx: X axis translation amount
        :param amounty: Y axis translation amount
        :return: None
        """
        self.glediste[0] += amountx
        self.glediste[1] += amounty

    def translate_towards(self, amount: float, vector: np.ndarray):
        """
        Translates the camera position towards a given vector. Used to translate the camera towards where it is
        currently looking
        :param amount: Amount by which to translate the camera
        :param vector: Vector along which the camera is to be translated
        :return: None
        """
        self.ociste += amount*vector

    def update_camera(self, dt):
        """
        Scheduled function which updates the camera position based on the current keystate (which keys are currently
        pressed)
        :param dt: required parameter by the pyglet.clock scheduler, not used.
        :return: None
        """
        vector = self.ociste - self.glediste
        vector = vector / np.linalg.norm(vector)
        if self.keystate[key.W]:
            self.translate_towards(-1*self.translation_amount, vector)
            #self.translate_camera(-1*self.translation_amount, "z")
        elif self.keystate[key.S]:
            self.translate_towards(self.translation_amount, vector)
            #self.translate_camera(self.translation_amount, "z")
        elif self.keystate[key.A]:
            self.translate_camera(-1*self.translation_amount, "x")
        elif self.keystate[key.D]:
            self.translate_camera(self.translation_amount, "x")
        elif self.keystate[key.Q]:
            self.translate_camera(self.translation_amount, "y")
        elif self.keystate[key.E]:
            self.translate_camera(-1*self.translation_amount, "y")
        elif self.keystate[key.RIGHT]:
            self.angle_rot_lr += self.angle_step
            self.rotate_camera(self.angle_rot_lr, "y")
        elif self.keystate[key.LEFT]:
            self.angle_rot_lr -= self.angle_step
            self.rotate_camera(self.angle_rot_lr, "y")
        elif self.keystate[key.UP]:
            self.angle_rot_ud -= self.angle_step
            self.rotate_camera(self.angle_rot_ud, "x")
        elif self.keystate[key.DOWN]:
            self.angle_rot_ud += self.angle_step
            self.rotate_camera(self.angle_rot_ud, "x")

