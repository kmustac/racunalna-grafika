from population import Population
from chromosome import Chromosome
from functions import *
import time
import random
import pyglet
from pyglet.gl import *
import numpy as np


class GeneticAlgorithm:
    """
    Class that models a genetic algorithm
    """
    def __init__(self, vel_pop, lb, ub, size, function):
        """
        Constructor takes in all the parameters needed for the population and its chromosomes aswell as the goal
        function.
        :param vel_pop: Size of the population
        :param lb: Lower bound for the chromosomes
        :param ub: Upper bound for the chromosomes
        :param size: Size of the chromosomes
        :param function: Function to be optimized
        """
        self.population = Population(vel_pop, lb, ub, size)
        self.last_pop = None
        self.function = function
        self.num_iter = 1
        self.new_best = self.get_bw_chrom(self.population.population, True)
        self.best_values = []

    def tournament_selection(self, k: int):
        """
        Simple k-tournament selection. Also immediately removes the worst chromosome from the population.
        :param k: How many chromosomes participate in the selection
        :return: 2 best chromosomes from the tournament.
        """
        selected = []
        while len(selected) < k:
            random_index = random.randint(0, self.population.vel_pop - 1)
            random_chrom = self.population.population[random_index]
            if random_chrom not in selected:
                selected.append(random_chrom)
        worst = self.get_bw_chrom(selected, False)
        self.population.remove_chromosome(worst)

        while len(selected) > 2:
            random_index = random.randint(0, len(selected) - 1)
            selected.remove(selected[random_index])
        return selected

    def get_bw_chrom(self, chrom_set, best: bool):
        """
        Gets the best or worst chromosome from a given set.
        :param chrom_set: Chromosome set
        :param best: Best = True, Worst = False
        :return: Best or worst chromosome from the given set
        """
        values = []
        for chrom in chrom_set:
            values.append(self.function(chrom.values))

        index = 0
        if best:
            index = values.index(min(values))
        else:
            index = values.index(max(values))
        return chrom_set[index]

    def run(self, max_iter, mutation_prob, num_tournament, k_turn, eta=1e-6, trace=True):
        """
        Runs the genetic algorithm with the given parameters
        :param max_iter: Max num of iterations
        :param mutation_prob: Mutation probability
        :param num_tournament: Number of times which the tournament is to be run
        :param k_turn: Parameter k for the tournament
        :param eta: Stopping point
        :param trace: Set to 'yes' to enable print statemets
        :return: Best chromosome that optimises the given function
        """
        self.num_iter = 1
        last_best = self.population.population[0]

        if trace:
            print("Pocetni najbolji kromosom")
            print(last_best)

        while self.num_iter <= max_iter and self.function(last_best.values) >= eta:
            for i in range(0, num_tournament):
                selected = self.tournament_selection(k_turn)
                pom = selected[0]

                child = pom.cross_arithmetic(selected[0], selected[1])
                child = pom.mutate_additive(child, mutation_prob)

                self.population.add_chromosome(child)

            new_best = self.get_bw_chrom(self.population.population, True)

            if self.function(new_best.values) < self.function(last_best.values):
                if trace:
                    print("Iteracija " + str(self.num_iter) + ". :")
                    print(str(new_best) + " funk: " + str(self.function(new_best.values)))

                last_best = new_best
            self.num_iter += 1
        return last_best, self.num_iter

    def run_once(self, max_iter, mutation_prob, num_tournament, k_turn, eta=1e-6, trace=True):
        """
        Runs only one iteration of the genetic algorithm with the given parameters
        :param max_iter: Max num of iterations
        :param mutation_prob: Mutation probability
        :param num_tournament: Number of times which the tournament is to be run
        :param k_turn: Parameter k for the tournament
        :param eta: Stopping point
        :param trace: Set to 'yes' to enable print statemets
        :return: Best chromosome that optimises the given function
        """
        last_best = self.get_bw_chrom(self.population.population, True)
        self.new_best = last_best
        self.best_values.append(last_best)

        if trace and self.num_iter == 1:
            print("Pocetni najbolji kromosom")
            print(last_best)

        if self.num_iter <= max_iter and abs(self.function(last_best.values) - self.function.min) >= eta:
            for i in range(0, num_tournament):
                selected = self.tournament_selection(k_turn)
                pom = selected[0]

                child = pom.cross_arithmetic(selected[0], selected[1])
                child = pom.mutate_additive(child, mutation_prob)

                self.population.add_chromosome(child)

            new_best = self.get_bw_chrom(self.population.population, True)
            self.old_best = last_best

            if self.function(new_best.values) < self.function(last_best.values):
                if trace:
                    print("Iteracija " + str(self.num_iter) + ". :")
                    print(str(new_best) + " funk: " + str(self.function(new_best.values)))

                self.new_best = new_best
                last_best = new_best
                self.best_values.append(last_best)
            self.num_iter += 1
        else:
            print(f"Najbolja jedinka = {last_best} nakon {self.num_iter} iteracija sa {self.function(last_best.values)} vrijednosti")
            print("Unscheduling...")
            pyglet.clock.unschedule(self.run_animation)
            print("Done...")

        return last_best

    def run_animation(self, dt):
        """
        Runs the animation of chromosomes
        :param dt: required parameter, not used
        :return: None
        """
        if self.function.name == "holder_table_function":
            eta = 1e-1
        else:
            eta = 1e-5
        max_iter = 10000
        mutation_prob = 0.3
        num_tournament = 10
        k_turn = 3
        trace = True

        self.run_once(max_iter, mutation_prob, num_tournament, k_turn, eta, trace)

    def draw_population(self):
        for chromosome in self.population.population:
            chromosome.draw(self.function)
        return self.get_bw_chrom(self.population.population, True)


if __name__ == "__main__":
    function = simple_function
    ga = GeneticAlgorithm(25, -5, 5, 2, function)

    best_chrom, num_iter = ga.run(10000, 0.3, 10, 3, eta=1e-6, trace=True)
    print(f"Najbolja jedinka = {best_chrom} nakon {num_iter} iteracija sa {function(best_chrom.values)} vrijednosti")
