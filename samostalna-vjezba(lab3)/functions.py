from math import sin, cos, exp

"""
Different functions for testing...
"""

def simple_function(x):
    return cos(((x[0])**2 + (x[1])**2)**0.5)


def quadratic_function(inputs):
    x = inputs[0]
    y = inputs[1]
    return x**2 + y**2


def three_hump_function(inputs):
    x = inputs[0]
    y = inputs[1]
    return 2*x**2 - 1.05*x**4 + (x**6)/6 + x*y + y**2


def rosenbrock_banana(inputs):
    x = inputs[0]
    y = inputs[1]
    return 100*(y - x**2)**2 + (1 - x)**2


def rastrigin_function(inputs):
    x = inputs[0]
    y = inputs[1]
    return 10*2 + x**2 - 10*cos(2*3.14*x) + y**2 - 10*cos(2*3.14*y)


def holder_table_function(inputs: list):
    x = inputs[0]
    y = inputs[1]
    pom = abs(1 - ((x**2 + y**2)**0.5)/3.14)
    return -1 * abs(sin(x) * cos(y) * exp(pom))


simple_function.name = "simple_function"
quadratic_function.name = "quadratic_function"
three_hump_function.name = "three_hump_function"
rosenbrock_banana.name = "rosenbrock_banana"
rastrigin_function.name = "rastrigin_function"
holder_table_function.name = "holder_table_function"


simple_function.min = -1
quadratic_function.min = 0
three_hump_function.min = 0
rosenbrock_banana.min = 0
rastrigin_function.min = 0
holder_table_function.min = -19.2085
