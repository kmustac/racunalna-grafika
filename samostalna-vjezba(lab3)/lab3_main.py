from camera import Camera
from terrain import Terrain
import functions as fcs
from geneticalg import GeneticAlgorithm
from chromosome import Chromosome
import time
import numpy as np
import pyglet
import _ctypes as ct
from pyglet.window import mouse, key
from pyglet.gl import *
from pyglet import image
from math import cos, sin
from math import *

config = pyglet.gl.Config(double_buffer=True)
window = pyglet.window.Window(config=config, width=1600, height=1200)


def coords():
    # x os je crvena
    glColor3f(1.0, 0.0, 0.0)
    glBegin(GL_LINES)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(3.0, 0.0, 0.0)
    glEnd()
    # y os je zelena
    glColor3f(0.0, 1.0, 0.0)
    glBegin(GL_LINES)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 3.0, 0.0)
    glEnd()
    # z os je plava
    glColor3f(0.0, 0.0, 1.0)
    glBegin(GL_LINES)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 0.0, 3.0)
    glEnd()
    glColor3f(1.0, 1.0, 1.0)


def loadBody(x):
    file = open(x, 'r')
    line = file.readline()
    kutevi = []
    poligoni = []
    while line:
        if line.startswith('v'):
            thing = line[2:-1]
            thing = thing.split(' ')
            kutevi.append((float(thing[0]), float(thing[1]), float(thing[2])))
        elif line.startswith('f'):
            thing = line[2:-1].strip()
            thing = thing.split(' ')
            poligoni.append((int(thing[0])-1, int(thing[1])-1, int(thing[2])-1))
        line = file.readline()
    file.close()
    return kutevi, poligoni


def draw_cube(x, y, z):
    glTranslatef(x, y, z)
    global kutevi, poligoni
    for poly in poligoni:
        glBegin(GL_POLYGON)
        for ind in poly:
            kut = kutevi[ind]
            glVertex3f(kut[0], kut[1], kut[2])
        glEnd()
    glTranslatef(-x, -y, -z)


def get_scaling():
    # Function scaling
    if function.name == "simple_function":
        sf_y = 1 / 5
        sf_x = 1 / 5
    elif function.name == "quadratic_function":
        sf_y = 1 / 40
        sf_x = 1 / 5
    elif function.name == "three_hump_function":
        sf_y = 1 / 100000
        sf_x = 1 / 5
    elif function.name == "rosenbrock_banana":
        sf_y = 1 / 200000
        sf_x = 1 / 5
    elif function.name == "rastrigin_function":
        sf_y = 1 / 100
        sf_x = 1 / 5
    elif function.name == "holder_table_function":
        sf_y = 1 / 10
        sf_x = 1 / 10
    return sf_y, sf_x


@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(35, width / float(height), .1, 1000)
    glMatrixMode(GL_MODELVIEW)
    return pyglet.event.EVENT_HANDLED


@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glClearColor(0.35, 0.35, 0.35, 1)
    glLoadIdentity()
    glEnable(GL_DEPTH_TEST)

    camx, camy, camz = camera.ociste[0], camera.ociste[1], camera.ociste[2]
    lookx, looky, lookz = camera.glediste[0], camera.glediste[1], camera.glediste[2]
    viewx, viewy, viewz = camera.view[0], camera.view[1], camera.view[2]
    gluLookAt(camx, camy, camz, lookx, looky, lookz, viewx, viewy, viewz)

    coords()

    sf_y, sf_x = get_scaling()
    glScalef(sf_x, sf_y, sf_x)
    function_plot.draw_terrain()
    glScalef(1 / sf_x, 1 / sf_y, 1 / sf_x)

    if keys[key.K]:
        a = 0
        for chrom in ga.best_values:
            color = [a, a, a]
            chrom.draw_color(function, color)
            a += 1/len(ga.best_values)
    elif keys[key.N]:
        a = 0
        interpolation = 25
        for i in range(len(ga.best_values)-1):
            color1 = [a for j in range(3)]
            first = ga.best_values[i]
            second = ga.best_values[i+1]

            x_range = (second.values[0] - first.values[0])/interpolation
            y_range = (second.values[1] - first.values[1])/interpolation
            for i in range(interpolation):
                helpBoi = Chromosome(first.lower_bound, first.upper_bound, first.size)
                helpBoi.values = [first.values[0] + x_range*i, first.values[1] + y_range*i]
                helpBoi.draw_color(function, color1)
            a += 1/len(ga.best_values)
    elif keys[key.M]:
        a = 0
        sf_k = 1 / 10
        glScalef(sf_k, sf_k, sf_k)
        glLineWidth(100)
        glBegin(GL_LINE_STRIP)
        for i in range(len(ga.best_values)):
            boi = ga.best_values[i].values
            glColor3f(a, a, a)
            glVertex3f(boi[0], function([boi[0], boi[1]]), boi[1])
            a += 1/len(ga.best_values)
        glEnd()
        glLineWidth(1)
        glScalef(1 / sf_k, 1 / sf_k, 1 / sf_k)
    elif keys[key.P]:
        pyglet.clock.unschedule(ga.run_animation)
    elif keys[key.O]:
        pyglet.clock.schedule_interval(ga.run_animation, 1/ga_freq)
    else:
        best_one = ga.draw_population()


@window.event
def on_mouse_drag(x, y, dx, dy, button, modifiers):
    global camera
    if button == mouse.LEFT:
        camera.translate_glediste(dx/30, dy/30)


if __name__ == "__main__":
    kutevi, poligoni = loadBody("kocka.obj")
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    angle_rot_lr, angle_rot_ud, rotation_axis = 0, 0, "y"
    keys = key.KeyStateHandler()
    window.push_handlers(keys)
    camera = Camera(keys)
    camera.set_ociste(3.0, 11.0, 15.0)
    camera.set_glediste(0.0, 0.0, 0.0)
    camera.set_viewup(0, 1, 0)

    range_x = (-10, 10)
    range_y = (-10, 10)
    function = fcs.holder_table_function
    function_plot = Terrain(range_x, range_y, function)

    range_c = (-10, 10)
    ga_freq = 20
    ga = GeneticAlgorithm(35, range_c[0], range_c[1], 2, function)

    pyglet.clock.schedule_interval(camera.update_camera, 1/144)
    pyglet.clock.schedule_interval(ga.run_animation, 1/ga_freq)
    pyglet.app.run()
