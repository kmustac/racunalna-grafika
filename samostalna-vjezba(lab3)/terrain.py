import numpy as np
from pyglet.gl import *
from math import *


class Terrain:
    """
    Class that generates the 3D terrain.
    """
    def __init__(self, range_x, range_y, terrain_func):
        """
        Generates the terrain in the given bounds using a given function.
        :param range_x: Bounds for the x axis
        :param range_y: Bounds for the z axis
        :param terrain_func: Function by which to generate the heightmap for the triangles that make up the terrain
        """
        self.terrain_func = terrain_func
        self.dots = 50
        self.range_x = np.linspace(range_x[0], range_x[1], self.dots)
        self.range_y = np.linspace(range_y[0], range_y[1], self.dots)

        self.color_orange = np.array([1, 165 / 255, 0])
        self.color_blue = np.array([65 / 255, 105 / 255, 1])

    def draw_terrain(self):
        """
        Draws the generated terrain and colors it.
        :return:
        """
        for i in range(len(self.range_x)-1):
            glBegin(GL_TRIANGLE_STRIP)
            for j in range(len(self.range_y)):
                x = self.range_x[i]
                y = self.range_y[j]
                xp1 = self.range_x[i+1]
                glColor3f(abs(y) / x ** 2, abs(x) / y ** 2, abs(x) / (y) ** 2)

                glVertex3f(x, self.terrain_func([x, y]), y)
                glVertex3f(xp1, self.terrain_func([xp1, y]), y)
            glEnd()

