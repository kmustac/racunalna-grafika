from chromosome import Chromosome


class Population:
    """
    Class that models the genetic algorithm population
    """
    def __init__(self, vel_pop, lb, ub, size):
        """
        Class constructor.
        :param vel_pop: Size of the population
        :param lb: Lower bound for the chromosomes
        :param ub: Upper bound for the chromosomes
        :param size: Size of the chromosomes
        """
        self.vel_pop = vel_pop
        self.lower_bound = lb
        self.upper_bound = ub
        self.size = size

        self.population = []
        for i in range(0, self.vel_pop):
            c = Chromosome(self.lower_bound, self.upper_bound, self.size)
            self.population.append(c)

    def remove_chromosome(self, chromosome_to_remove):
        """
        Remove a chromosome from the population
        :param chromosome_to_remove: Chromosome to be removed from the population
        :return:
        """
        self.population.remove(chromosome_to_remove)

    def add_chromosome(self, chromosome_to_add):
        """
        Adds a new chromosome to the population
        :param chromosome_to_add: Chromosome to be added to the population
        :return:
        """
        self.population.append(chromosome_to_add)
